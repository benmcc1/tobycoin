coin_diameter = 35;
coin_thickness = 1;

rim_width = 1;
rim_height = 0.5;

toby_box_diameter = 25;

depth_brown = 0.4;
depth_orange = 0.2;

module base_coin() {
    union() {
        linear_extrude(height = coin_thickness) {
            circle(d= coin_diameter, $fn=360);
        }
        translate([0, 0, -rim_height])
        linear_extrude(height = coin_thickness + (rim_height * 2)) {
            difference() {
                circle(d = coin_diameter, $fn = 360);
                circle(d = coin_diameter - rim_width, $fn = 360);
            }
        }
        
    }
}

module toby_brown() {
    color("brown", 1.0)
    translate([
        -coin_diameter / 2,
        -coin_diameter / 2,
        0
    ])
    linear_extrude(height = depth_brown) {
        import("tobes.brown.svg");
    };
}
module toby_orange() {
    color("orange", 1.0)
    translate([
        -coin_diameter / 2,
        -coin_diameter / 2,
        0
    ])
    linear_extrude(height=depth_orange) {
        import("tobes.orange.svg");
    };
}
module face_text() {
    color("purple", 1.0)
        translate([
        -coin_diameter / 2,
        -coin_diameter / 2,
        0
    ])
    translate([0, 0, coin_thickness])
    linear_extrude(height=depth_orange) {
        import("text.svg");
    };
}

module toby() {
    translate([0, 0, coin_thickness])
    rotate([0, 0, 0])
    union() {
        toby_brown();
        toby_orange();
    }
}

color("gold", 1.0)
union() {
    toby();
    face_text();
    base_coin();
}
